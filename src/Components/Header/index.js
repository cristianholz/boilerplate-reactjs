import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <>
      <header>
        <div className="row">
          <Link to="/">Home</Link>
          <Link to="/sobre">Sobre</Link>
        </div>
      </header>
    </>
  );
};

export default Header;
