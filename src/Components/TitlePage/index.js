import React from "react";
import Helmet from "react-helmet";

const TitlePage = ({ title }) => {
  var defaultTitle = "App";
  return (
    <Helmet titleTemplate="%s - App" defaultTitle="App">
      <title>{title ? title : defaultTitle}</title>
    </Helmet>
  );
};

export default TitlePage;
