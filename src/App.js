import React from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

//SASS GLOBAL
import './Styles/App.scss'

//COMPONENTS
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import TitlePage from "./Components/TitlePage";

//PAGES
import Home from "./Pages/Home";
import NotFoundPage from "./Pages/NotFoundPage";
import Sobre from "./Pages/Sobre";

function App() {
  return (
    <Router>
      <>
        <TitlePage title="Sobre" />
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/sobre" component={Sobre} />
          <Route path="*" component={NotFoundPage} />
        </Switch>
        <Footer />
      </>
    </Router>
  );
};

export default App;
